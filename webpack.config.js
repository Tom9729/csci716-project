const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

// ThreeJS examples help from:
// https://gist.github.com/cecilemuller/0be98dcbb0c7efff64762919ca486a59

module.exports = {
    resolve: {
	alias: {
            'three$': 'three/build/three.min.js',
            'three/.*$': 'three',
	}
    },
    output: {
        path: __dirname + '/dist',
        filename: 'index_bundle.js'
    },
    plugins: [
        new HtmlWebpackPlugin({
            title: "Landscapes Demo"
        }),
        new webpack.ProvidePlugin({
	    'THREE': 'three'
	})
    ]
};
