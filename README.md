# Computer Generated Landscapes

https://www.cs.rit.edu/~tca4384/csci716/

## Requirements

- NodeJS 8 w/ NPM
- Browser that supports WebGL

## Usage

Start the dev server which will host the app and run webpack to compile it whenever resources change.

```
$ npm run dev
```

Then go to `http://localahost:8080`.

Use WSAD keys and mouse to fly around.

To build a standalone version run build then upload to server.

```
$ npm run build
$ scp -r dist user@joplin.cs.rit.edu:./public_html/
```

To generate a different terrain tweak the parameters at the top of `terrain.py` and then run it and redirect
the output to overwrite `terrain.js`.

```
$ ./terrain.py > src/terrain.js
```
