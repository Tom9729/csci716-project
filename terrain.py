#!/usr/bin/env python

import json
import random
import sys

def pointOrientation(p, q, r):
    """
    Determine orientation by looking at sign of determinant, as described in
    CMSC 754 lecture notes and Wikipedia. First column has been dropped
    because it does not affect the result.
    
    - If output is POSITIVE then R is to the LEFT of PQ.
    - If output is NEGATIVE than R is to the RIGHT of PQ.
    - If output is ZERO then R is collinear with PQ.

    https://www.cs.umd.edu/class/spring2012/cmsc754/Lects/cmsc754-lects.pdf
    https://en.wikipedia.org/wiki/Determinant#3_%C3%97_3_matrices
    """
    (px, py) = p
    (qx, qy) = q
    (rx, ry) = r
    (b, c, e, f, h, i) = (px, py, qx, qy, rx, ry)
    det = e * i + b * f + c * h - c * e - b * i - f * h
    return det

patchesSize = 30
patchesTerrainSize = 16 + 1
iterations = 2000;

terrainSize = patchesSize * patchesTerrainSize - 1

vertices = []

rowColToIdx = lambda m, n: m * terrainSize + n
nextTerrainCoord = lambda: random.random() * terrainSize

for m in range(0, terrainSize):
    for n in range(0, terrainSize):
        vertices.append([n, 0, m])
        
for i in range(0, iterations):
    p = (nextTerrainCoord(), nextTerrainCoord())
    q = (nextTerrainCoord(), nextTerrainCoord())

    for m in range(0, terrainSize):
        for n in range(0, terrainSize):
            r = (m, n)
            det = pointOrientation(p, q, r)
            raisev = (1 if det > 0 else -1) * 0.2
            i = rowColToIdx(m, n)
            vertices[i][1] += raisev

output = {
    "patchesSize": patchesSize,
    "patchesTerrainSize": patchesTerrainSize,
    "terrainSize": terrainSize,
    "vertices": vertices
}
            
print("export const data = " + json.dumps(output) + ";")
