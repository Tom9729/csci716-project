/**
 * CSCI-716 Project - Computer Generated Landscapes
 * Tom Arnold <tca4384@rit.edu>
 * 
 * Focus:
 * 1. Heightmap renderer random points
 * 2. Interesting terrain algs (point diamond, faults)
 * 3. LOD renderer (patches, bintri trees)
 *
 * https://www.gamasutra.com/view/feature/130171/binary_triangle_trees_for_terrain_.php?page=4
 * https://www.longbowgames.com/seumas/progbintri.html
 * https://sourceforge.net/p/dagger3d/code/HEAD/tree/trunk/dagger3d/examples/terrain_lod3.c#l125
 *
 * TODO list
 * - btt data structure for better lod
 * - start all patches on min LOD find nearest patch,
 *   walk outward decreasing LOD based on distance until
 *   min reached
 */
import * as THREE from "three";
import "three/examples/js/controls/FlyControls";
import "three/examples/js/controls/FirstPersonControls";
import {rowColToIdx} from "./math";
import * as TERRAIN from "./terrain.js";

// Distance to far plane, for clipping.
let farDist = 600000;

let camera = new THREE.PerspectiveCamera(45, window.innerWidth / window.innerHeight, 1000, farDist);

let scene = new THREE.Scene();

// Adjusts the movement speed of the camera.
let movementSpeed = 1000;

// Adjusts how large the terrain is (scaled) and the minimum distance (in patches)
// for each resolution level.
let scaleXZ = 800;
let scaleY = 600;
let levelThresholds = [
    4, // Level 0
    6, // Level 1
    8, // Level 2
    10 // Level 3
];

let data = TERRAIN.data;

console.log("Loaded " + data.vertices.length + " vertices.");

let patchesSize = data.patchesSize;

// MUST be powers of 2 plus 1.
let patchesTerrainSize = data.patchesTerrainSize;
let terrainSize = data.terrainSize;

let meshCenterCoord = terrainSize * scaleXZ;

// Put camera roughly in center of terrain.
camera.position.set(meshCenterCoord / 2, 30000, meshCenterCoord / 2);
camera.lookAt(0, 20, 10);

let time = () => new Date().getTime();

let delta = (t) => time() - t;

let start = 0;

// At this point we have terrain data stored as a renderable ThreeJS geometry.
// Next we need to split it into patches that can be rendered at different resolutions.

let patches = [];
let patchesByRC = {};
let patchRCById = {};

start = time();

// Iterate over patch rows/columns.
// This is a bit tricky because we are translating from one grid resolution
// to another, and then to a 1D array.
for (let s = 0; s < patchesSize; ++s) {
    for (let t = 0; t < patchesSize; ++t) {

        let patch = new THREE.Geometry();
        let patchIdx = rowColToIdx(s, t, patchesSize);

        let mOffset = s * (patchesTerrainSize - 1);
        let nOffset = t * (patchesTerrainSize - 1);
        
        for (let m = 0; m < patchesTerrainSize; ++m) {
            for (let n = 0; n < patchesTerrainSize; ++n) {

                let m2 = m + mOffset;
                let n2 = n + nOffset;

                let i = rowColToIdx(m2, n2, terrainSize);
                let v = data.vertices[i];
                
                patch.vertices.push(new THREE.Vector3(v[0], v[1], v[2]));
            }
        }

        patch.scale(scaleXZ, scaleY, scaleXZ);
        patch.computeBoundingSphere();
        patch.computeBoundingBox();

        patches.push(patch);
        patchesByRC[[s, t]] = patch;
        patchRCById[patch.id] = [s, t];
    }
}

console.log("Splitting terrain took " + delta(start) + "ms");

start = time();

let computeFaces = (patch, level) => {
    patch.faces.length = 0;

    // Fill terrain faces. Every 4 points become 2 triangles.
    //
    //   A-D-*
    //   |\|\|
    //   B-C-*
    //   |\|\|
    //   *-*-*
    // MUST be power of two.
    let stride = Math.pow(2, level);
    if (stride >= patchesTerrainSize - 1) {
        stride = patchesTerrainSize - 1;
    }
    for (let m = 0; m < patchesTerrainSize - 1; m += stride) {
        for (let n = 0; n < patchesTerrainSize - 1; n += stride) {

            let a = rowColToIdx(m, n, patchesTerrainSize);
            let b = rowColToIdx(m + stride, n, patchesTerrainSize);
            let c = rowColToIdx(m + stride, n + stride, patchesTerrainSize);
            let d = rowColToIdx(m, n + stride, patchesTerrainSize);

            patch.faces.push(new THREE.Face3(a, b, c));
            patch.faces.push(new THREE.Face3(a, c, d));
        }
    }

    // Signal to Three that we updated indices.
    patch.elementsNeedUpdate = true;
};

// Regenerate a wireframe mesh for another mesh. These are tracked in a map outside
// of the scene for easy replacement when we need to regenerate
let wireframes = {};
let updateWireframe = patch => {
    // Render wireframe overlay
    // https://stackoverflow.com/a/31541369
    //
    // It seems like a bug in Three but we get an error with the wireframe mesh
    // if we try to adjust the face indices. This works for the regular mesh however.
    // Sounds like https://github.com/mrdoob/three.js/pull/15198 but I tried the fix
    // there and it did not help, so as an (ugly) workaround we need to clone the
    // mesh everytime the faces change.
    let wireframe = wireframes[patch.id];
    if (wireframe) {
        scene.remove(wireframe);
        wireframe.geometry.dispose();
    }
    wireframe = new THREE.Mesh(patch.clone(), new THREE.MeshBasicMaterial({
        color: "#452b43",
        wireframe: true
    }));
    wireframes[patch.id] = wireframe;
    scene.add(wireframe);
};

patches.forEach((patch, idx) => {

    computeFaces(patch, idx);

    let mesh = new THREE.Mesh(patch, new THREE.MeshLambertMaterial({
        color: "#080808",
        shading: THREE.SmoothShading
    }));
    
    scene.add(mesh);

    updateWireframe(patch);
});

console.log("Processing patches took " + delta(start) + "ms");

let posToGrid = pos => {
    let size = terrainSize * scaleXZ;
    let num = patchesSize;
        
    let m = Math.round((pos.z / size) * num);
    let n = Math.round((pos.x / size) * num);

    let height = Math.round(pos.y / (patchesTerrainSize * scaleXZ));
    
    return [m, height, n];    
};

// Go through every patch and set its level of detail based on the distance (in patches)
// from the camera. Several optimizations are in place to avoid recalculating things if
// not enough time has passed, or if nothing has changed.
let lastPos = null;
let lastUpdate = time();
let levels = {};
let updateLod = () => {

    // Avoid thrashing by forcing a 50ms buffer here.
    if (time() - lastUpdate < 50) {
        return;
    }

    lastUpdate = time();

    // Only update LOD if the camera position (in patch-grid coords) has changed.
    let pos = posToGrid(camera.position);
    if (lastPos && lastPos[0] === pos[0] && lastPos[1] === pos[1] && lastPos[2] === pos[2]) {
        return;
    }
    lastPos = pos;
    let [s2, h2, t2] = pos;

    // Loop through each patch, calculate new level, and then update it.
    patches.forEach(patch => {

        let level = levelThresholds.length;
        let [s, t] = patchRCById[patch.id];

        let dist = Math.sqrt(Math.pow(s2 - s, 2) +
                             Math.pow(h2, 2) +
                             Math.pow(t2 - t, 2));

        for (let i = 0; i < levelThresholds.length; ++i) {
            let threshold = levelThresholds[i];
            if (dist < threshold) {
                level = i;
                break;
            }
        }

        // Recalculate mesh faces and wireframe if level has changed. Depending on thresholds
        // it's possible that we could move and have the same level so we should avoid recalculating
        // faces (and especially the wireframe mesh).
        let currentLevel = levels[patch.id];
        if (currentLevel !== level) {
            computeFaces(patch, level);
            updateWireframe(patch);
            levels[patch.id] = level;
        }
    });

    console.log("Updating LOD took " + delta(lastUpdate) + "ms");
};

scene.add(new THREE.AmbientLight("white"));

// Create WebGL canvas.
let renderer = new THREE.WebGLRenderer({
    antialias: true
});
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

// Fix extra margin that shows up for some reason.
let body = document.querySelector("body");
body.style.margin = 0;
body.style.overflow = "hidden";

// Use FPS controls, mouse and WSAD.
let controls = new THREE.FirstPersonControls(camera, body);
controls.movementSpeed = movementSpeed;

// Set initial resolutions for each patch.
updateLod();

// WebGL render callback.
let render = function () {
    renderer.clear();
    renderer.render(scene, camera);
};

// WebGL window resize callback.
let resize = function () {
    let width = window.innerWidth;
    let height = window.innerHeight;
    camera.aspect = width / height;
    camera.updateProjectionMatrix();
    renderer.setSize(width, height);
};

// WebGL animation callback.
let animate = function() {
    requestAnimationFrame(animate);
    render();
    controls.update(0.5);
    updateLod();
};

animate();

window.addEventListener("resize", resize, false);
