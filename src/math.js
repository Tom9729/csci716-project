export function rowColToIdx(m, n, size) {
    return (m * size) + n;
}
